package main

import (
	"fmt"
)

func isBalanced(term string) bool {
	var balance int

	for _, char := range term {
		switch char {
		case '(':
			balance++
		case ')':
			balance--
			if balance < 0 {
				return false
			}
		}
	}

	return balance == 0
}

func checkTerms(terms []string) {
	for _, term := range terms {
		if isBalanced(term) {
			fmt.Printf("Der Term \"%s\" hat korrekte Klammern.\n", term)
		} else {
			fmt.Printf("Der Term \"%s\" hat nicht korrekte Klammern.\n", term)
		}
	}
}

func main() {
	terms := []string{
		"(a + (b - c) * (d / e))",
		"(a + b) - (c * d))",
		"((a + b) * (c - d)",
		"(a + b) * (c - d)",
	}
	checkTerms(terms)
}
