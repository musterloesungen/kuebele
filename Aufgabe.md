# Aufgabe: Parallelisiertes Laden von Webinhalten mit Go

## Ziel
Schreibe ein Go-Programm, das parallel Inhalte von einer Liste von URLs herunterlädt und diese in einer Map speichert. Die HTTP-Anfragen sollen parallel in Go-Routinen ausgeführt werden. Die Map soll die URL als Key und den heruntergeladenen Inhalt als Value enthalten.

## Anforderungen

1. **HTTP-Anfragen parallel ausführen:**
   - Verwende Go-Routinen, um die HTTP-Anfragen parallel auszuführen.
   - Nutze einen Channel, um die heruntergeladenen Inhalte zu kommunizieren.

2. **Daten speichern:**
   - Speichere die heruntergeladenen Inhalte in einer Map.
   - Die URL soll als Key und der Inhalt als Value in der Map gespeichert werden.

3. **Synchronisierung:**
   - Verwende nur Channels zur Synchronisation der Go-Routinen.
   - Stelle sicher, dass das Programm auf alle Go-Routinen wartet, bevor es beendet wird.

## Hinweise

- Nutze die Bibliothek `net/http` für HTTP-Anfragen.
- Verwende eine einfache Map für die Speicherung der Ergebnisse.
