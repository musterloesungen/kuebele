package main

import (
	"fmt"
)

func isBalanced(term string) bool {
	var balance int

	for _, char := range term {
		switch char {
		case '(':
			balance++
		case ')':
			balance--
			if balance < 0 {
				return false
			}
		}
	}

	return balance == 0
}

func checkTerm(term string, results chan<- string) {
	if isBalanced(term) {
		results <- fmt.Sprintf("Der Term \"%s\" hat korrekte Klammern.", term)
	} else {
		results <- fmt.Sprintf("Der Term \"%s\" hat nicht korrekte Klammern.", term)
	}
}

func checkTerms(terms []string) {
	results := make(chan string, len(terms))

	for _, term := range terms {
		go checkTerm(term, results)
	}

	for range terms {
		fmt.Println(<-results)
	}
}

func main() {
	terms := []string{
		"(a + (b - c) * (d / e))",
		"(a + b) - (c * d))",
		"((a + b) * (c - d)",
		"(a + b) * (c - d)",
	}
	checkTerms(terms)
}
