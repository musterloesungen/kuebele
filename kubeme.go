package main

import (
	"fmt"
	"os"
	"path/filepath"
	"runtime"

	"github.com/olekukonko/tablewriter"
	"gopkg.in/yaml.v2"
)

type KubeConfig struct {
	Clusters []struct {
		Name    string `yaml:"name"`
		Cluster struct {
			Server string `yaml:"server"`
		} `yaml:"cluster"`
	} `yaml:"clusters"`
	Contexts []struct {
		Name    string `yaml:"name"`
		Context struct {
			Cluster string `yaml:"cluster"`
			User    string `yaml:"user"`
		} `yaml:"context"`
	} `yaml:"contexts"`
	CurrentContext string `yaml:"current-context"`
}

func main() {
	homeDir := getHomeDir()
	defaultKubeConfigPath := filepath.Join(homeDir, ".kube", "config")
	kubeConfigPaths := getConfigPaths(defaultKubeConfigPath)
	kubeConfigs := getKubeConfigs(kubeConfigPaths)
	printCombinedTable(kubeConfigPaths, kubeConfigs)
}

func getConfigPaths(defaultKubeConfigPath string) []string {
	var kubeConfigPaths []string
	if path, exists := os.LookupEnv("KUBECONFIG"); exists {
		// Wenn die Umgebungsvariable KUBECONFIG gesetzt ist, kann sie mehrere Pfade enthalten.
		kubeConfigPaths = append(kubeConfigPaths, filepath.SplitList(path)...)
		fmt.Println("KUBECONFIG: ", path)
	} else {
		// Standardpfad hinzufügen, falls keine Umgebungsvariable KUBECONFIG vorhanden ist.
		kubeConfigPaths = append(kubeConfigPaths, defaultKubeConfigPath)
		fmt.Println("KUBECONFIG ist nicht gesetzt. Verwende den Standardpfad:", defaultKubeConfigPath)
	}
	return kubeConfigPaths
}

func getKubeConfigs(kubeConfigPaths []string) map[string]KubeConfig {
	kubeConfigs := make(map[string]KubeConfig)
	for _, path := range kubeConfigPaths {
		if checkKubeConfigPath(path) {
			kubeConfig, err := readKubeConfig(path)
			if err == nil {
				kubeConfigs[path] = kubeConfig
			} else {
				fmt.Printf("Fehler beim Lesen der Datei: %v\n", err)
			}
		} else {
			fmt.Printf("Nicht gefunden: %s\n", path)
		}
	}
	return kubeConfigs
}

// Gibt das Home-Verzeichnis des Benutzers zurück.
func getHomeDir() string {
	if runtime.GOOS == "windows" {
		return os.Getenv("USERPROFILE")
	} else {
		return os.Getenv("HOME")
	}
}

// Überprüft, ob eine kubeconfig-Datei im angegebenen Pfad existiert.
func checkKubeConfigPath(path string) bool {
	_, err := os.Stat(path)
	return !os.IsNotExist(err)
}

// Liest und gibt den Inhalt der kubeconfig-Datei aus.
func readKubeConfig(path string) (KubeConfig, error) {
	content, err := os.ReadFile(path)
	if err != nil {
		return KubeConfig{}, err
	}

	var kubeConfig KubeConfig
	err = yaml.Unmarshal(content, &kubeConfig)
	if err != nil {
		return KubeConfig{}, err
	}

	return kubeConfig, nil
}

// Gibt eine kombinierte Tabelle der Cluster- und Kontextinformationen aus.
func printCombinedTable(kubeConfigPaths []string, kubeConfigs map[string]KubeConfig) {
	table := tablewriter.NewWriter(os.Stdout)
	table.SetHeader([]string{"Datei", "Cluster Name", "Cluster Server", "Context Name", "User"})

	// Fülle die Tabelle mit Daten
	for path, kubeConfig := range kubeConfigs {
		addRow(kubeConfig, table, path)
	}

	table.Render()
}

func addRow(kubeConfig KubeConfig, table *tablewriter.Table, path string) {
	clusterServerMap := make(map[string]string)
	for _, cluster := range kubeConfig.Clusters {
		clusterServerMap[cluster.Name] = cluster.Cluster.Server
	}

	for _, context := range kubeConfig.Contexts {
		clusterServer := clusterServerMap[context.Context.Cluster]
		contextName := context.Name
		if contextName == kubeConfig.CurrentContext {
			contextName += " (current)"
		}
		table.Append([]string{path, context.Context.Cluster, clusterServer, contextName, context.Context.User})
	}
}
